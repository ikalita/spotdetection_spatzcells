# SpotDetection_spatzcells

This project contains a modification of spatzcells.mat (MATLAB script) used for detection of single-molecule fluorescent HaloTag foci in bacteria _Escherichia coli_. 

In order to use the modified version, the full Spatzcells package needs to be downloaded from [Resources in Golding Lab @ UIUC](https://bacteriophysics.web.illinois.edu/resources/) and the original file spatzcells.m should be replaced with the modified file spatzcells_halo.m. The spot detection parameters may need to be adjusted for a particular setup, labelling protocol and microscopy conditions.


